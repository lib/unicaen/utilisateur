<?php

namespace UnicaenUtilisateur\ORM\Event\Listeners;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationService;
use UnicaenUtilisateur\Entity\Db\User;

class HistoriqueListenerFactory
{
    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): HistoriqueListener
    {
        /** @var AuthenticationService $authenticationService */
        $authenticationService = $container->get('Laminas\Authentication\AuthenticationService');

        $config = $container->get('Configuration');
        $userEntityClass = $config['zfcuser']['user_entity_class'] ?? User::class;
        $userId = $config['unicaen-utilisateur']['default-user'] ?? null;

        $listener = new HistoriqueListener();
        $listener->setAuthenticationService($authenticationService);
        $listener->setUserEntityClass($userEntityClass);
        $listener->setDefaultUser($userId);

        return $listener;
    }
}
