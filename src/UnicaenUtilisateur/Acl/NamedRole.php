<?php
namespace UnicaenUtilisateur\Acl;

use BjyAuthorize\Acl\Role;
use Laminas\Permissions\Acl\Role\RoleInterface;

/**
 * Rôle avec nom explicite (humainement intelligible).
 */
class NamedRole extends Role
{
    /**
     * @var string
     */
    protected $roleName;
    
    /**
     * @var string
     */
    protected $roleDescription;

    /**
     * @var bool
     */
    protected $selectable = true;
    

    /**
     * Constructeur.
     * 
     * @param string|null               $id
     * @param RoleInterface|string|null $parent
     * @param string|null               $name
     * @param string|null               $description
     * @param bool                      $selectable
     */
    public function __construct($id = null, $parent = null, ?string $name = null, ?string  $description = null, bool $selectable = true)
    {
        parent::__construct($id, $parent);
        
        $this
                ->setRoleName($name ?: $id)
                ->setRoleDescription($description ?: null)
                ->setSelectable($selectable);
    }

    /**
     * Retourne la représentation littérale de ce rôle.
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->getRoleName() ?: $this->getRoleId();
    }
    
    /**
     * Retourne le nom du rôle.
     * 
     * @return string
     */
    public function getRoleName() : string
    {
        return $this->roleName;
    }

    /**
     * Spécifie le nom du rôle.
     * 
     * @param string $roleName
     * @return self
     */
    public function setRoleName(string $roleName) : NamedRole
    {
        $this->roleName = $roleName;
        return $this;
    }

    /**
     * Retourne la description du rôle.
     * 
     * @return string
     */
    public function getRoleDescription() : string
    {
        return $this->roleDescription;
    }

    /**
     * Spécifie la description du rôle.
     * 
     * @param string|null $roleDescription
     * @return self
     */
    public function setRoleDescription(?string $roleDescription) : NamedRole
    {
        $this->roleDescription = $roleDescription;
        return $this;
    }
    
    /**
     * Teste si ce rôle est sélectionnable par l'utilisateur. 
     * Cela concerne les applications qui permettent à l'utilisateur de choisir son profil courant
     * parmi les différents rôles qu'il possède.
     * 
     * @return bool
     */
    public function getSelectable() : bool
    {
        return $this->selectable;
    }

    /**
     * Spécifie si ce rôle est sélectionnable par l'utilisateur. 
     * Cela concerne les applications qui permettent à l'utilisateur de choisir son profil courant
     * parmi les différents rôles qu'il possède.
     * 
     * @param bool $selectable
     * @return NamedRole
     */
    public function setSelectable(bool $selectable = true) : NamedRole
    {
        $this->selectable = $selectable;
        return $this;
    }
}