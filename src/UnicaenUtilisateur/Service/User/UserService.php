<?php

namespace UnicaenUtilisateur\Service\User;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Exception\NotSupported;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use InvalidArgumentException;
use Laminas\Authentication\AuthenticationService;
use ZfcUser\Password\Bcrypt;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Renderer\PhpRenderer;
use Ramsey\Uuid\Uuid;
use UnicaenApp\Entity\Ldap\People;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenAuthentification\Service\Traits\UserContextServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenUtilisateur\Controller\UtilisateurController;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Exception\RuntimeException;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;

class UserService implements RechercheIndividuServiceInterface
{
    use EntityManagerAwareTrait;
    use UserContextServiceAwareTrait;
    use RoleServiceAwareTrait;
    use MailServiceAwareTrait;

    private ?string $userEntityClass = null;
    private ?array $authentificationConfig = null;
    private ?int $passwordCost = null;
    private ?PhpRenderer $renderer = null;
    private ?string $appname = null;
    private ?AuthenticationService $authenticationService = null;
    private array $searchServices = [];

    public function setSearchServices(array $searchServices): void
    {
        $this->searchServices = $searchServices;
    }

    public function getAuthentificationConfig(): ?array
    {
        return $this->authentificationConfig;
    }

    public function setAuthentificationConfig(?array $authentificationConfig): void
    {
        $this->authentificationConfig = $authentificationConfig;
    }

    public function setRenderer(?PhpRenderer $renderer): void
    {
        $this->renderer = $renderer;
    }

    public function setAppname(?string $appname): void
    {
        $this->appname = $appname;
    }

    public function setUserEntityClass(string $userEntityClass): void
    {
        if (!class_exists($userEntityClass) || !in_array(UserInterface::class, class_implements($userEntityClass))) {
            throw new InvalidArgumentException("L'entité associée aux utilisateurs doit implémenter " . UserInterface::class);
        }
        $this->userEntityClass = $userEntityClass;
    }

    public function getEntityClass(): ?string
    {
        return $this->userEntityClass;
    }

    public function getEntityInstance(): UserInterface
    {
        return new $this->userEntityClass;
    }

    public function setAuthenticationService(AuthenticationService $authenticationService): void
    {
        $this->authenticationService = $authenticationService;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authenticationService;
    }

    public function getRepo(): EntityRepository
    {
        try {
            return $this->getEntityManager()->getRepository($this->userEntityClass);
        } catch (NotSupported $e) {
            throw new RuntimeException("Une erreur s'est produite lors de la récupération du repository [".$this->userEntityClass."]",0,$e);
        }
    }

    /** Gestion des entités  ******************************************************************************************/

    public function create(UserInterface $user): UserInterface
    {
        try {
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush($user);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la création de l'utilisateur.", null, $e);
        }
        return $user;
    }

    /**
     * @param UserInterface $utilisateur
     * @return UserInterface
     */
    public function update(UserInterface $utilisateur): UserInterface
    {
        try {
            $this->getEntityManager()->flush($utilisateur);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la mise à jour de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }
        return $utilisateur;
    }

    public function delete(UserInterface $utilisateur): UserInterface
    {
        try {
            $this->getEntityManager()->remove($utilisateur);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }
        return $utilisateur;
    }

    /** Requetage *****************************************************************************************************/

    public function find(mixed $id): ?UserInterface
    {
        return $this->getRepo()->find($id);
    }

    /** @return UserInterface[] */
    public function findAll(array $orderBy = ['displayName' => 'ASC']): array
    {
        return $this->getRepo()->findBy([], $orderBy);
    }

    public function findByUsername(string $username): ?UserInterface
    {
        return $this->getRepo()->findOneBy(['username' => $username]);
    }

    public function findById(mixed $id): ?UserInterface
    {
        return $this->find($id);
    }

    /** @return UserInterface[] * */
    public function findByTerm(string $term): array
    {
        $qb = $this->getRepo()->createQueryBuilder("u");
        $qb->andWhere(
            $qb->expr()->orX(
                "LOWER(u.displayName) LIKE :term",
                "LOWER(u.username) LIKE :term"
            )
        )
            ->setParameter("term", '%' . strtolower($term) . '%')
            ->orderBy("u.displayName");

        $utilisateurs = $qb->getQuery()->getResult();

        return $utilisateurs;
    }

    /** @return UserInterface[] */
    public function findByTermAndRole(string $texte, string $roleId): array
    {
        $texte = strtolower($texte);
        $qb = $this->getRepo()->createQueryBuilder("u")
            ->andWhere("LOWER(u.displayName) LIKE :critere")
            ->setParameter("critere", '%' . strtolower($texte) . '%')
            ->addSelect('r')->join('u.roles', 'r')
            ->andWhere('r.roleId = :role')
            ->setParameter("role", $roleId)
            ->orderBy("u.displayName");

        $utilisateurs = $qb->getQuery()->getResult();

        return $utilisateurs;
    }



    public function getRequestedUser(AbstractActionController $controller, string $paramName = 'utilisateur'): ?UserInterface
    {
        $id = $controller->params()->fromRoute($paramName);
        if ($id === null) return null;
        $user = $this->find($id);

        return $user;
    }

    /** @return UserInterface[] */
    public function findByRole(RoleInterface $role): array
    {
        $qb = $this->getRepo()->createQueryBuilder('u')
            ->addSelect('r')->join('u.roles', 'r')
            ->andWhere('r.roleId = :role')
            ->setParameter('role', $role->getRoleId());

        return $qb->getQuery()->getResult();
    }

    /** @return AbstractUser[] */
    public function getUtilisateursByTermAndRoleId(string $term, string $roleCode): array
    {
        $qb = $this->getRepo()->createQueryBuilder('u')
            ->addSelect('r')->join('u.roles', 'r')
            ->andWhere('r.roleId like :role')->setParameter('role', '%' . $roleCode . '%')
            ->andWhere('LOWER(u.displayName) like :term')->setParameter('term', '%' . strtolower($term) . '%');

        return $qb->getQuery()->getResult();
    }

    /** FACADE  *******************************************************************************************************/

    public function importFromRechercheIndividuResultatInterface(RechercheIndividuResultatInterface $individu, string $source): UserInterface
    {
        $attribut = $this->getAuthentificationConfig()['local']['ldap']['username'];

        $utilisateur = $this->getEntityInstance();
        $utilisateur->setDisplayName($individu->getDisplayname());
        $utilisateur->setUsername($individu->getUsername($attribut));
        $utilisateur->setEmail($individu->getEmail());
        $utilisateur->setPassword($source);
        $utilisateur->setState(1);

        return $utilisateur;
    }

    public function createLocal(UserInterface $user): UserInterface
    {
        $user->setState(1);
        $user->setPassword('db');
        try {
            $token = Uuid::uuid4()->toString();
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur s'est produite lors de la génération du token.", null, $e);

        }
        $user->setPasswordResetToken($token);
        $this->create($user);


        $appname = $this->appname;
        /** @see UtilisateurController::changerMotDePasseAction() */
        $url = $this->renderer->url('unicaen-utilisateur/changer-mot-de-passe', ['user' => $user->getId(), 'token' => $user->getPasswordResetToken()], ['force_canonical' => true], true);
        $subject = "Initialisation de votre compte pour l'application" . $appname;
        $body  = "<p>Bonjour " . $user->getDisplayName() . ",</p>";
        $body .= "<p>Pour initialiser votre mot de passe pour l'application " . $appname . " veuillez suivre le lien suivant <a href='" . $url . "'>" . $url . "</a>.</p>";
        $body .= "<p>Vous aurez besoin de votre identifiant qui est <strong>" . $user->getUsername() . "</strong></p>";
        $this->getMailService()->sendMail($user->getEmail(), $subject, $body);

        return $user;
    }

    public function setPasswordCost(?int $passwordCost): void
    {
        $this->passwordCost = $passwordCost;
    }

    public function changerPassword(User $user, $data): User
    {
        $password = $data['password1'];

        $bcrypt = new Bcrypt();
        if (isset($this->passwordCost)) {
            $bcrypt->setCost($this->passwordCost);
        }
        $password = $bcrypt->create($password);
        $user->setPassword($password);
        $user->setPasswordResetToken(null);
        $this->update($user);
        return $user;
    }

    public function changeStatus(UserInterface $utilisateur): UserInterface
    {
        $status = $utilisateur->getState();
        $utilisateur->setState($status == 1 ? 0 : 1);

        try {
            $this->getEntityManager()->flush($utilisateur);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors du changement de statut de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    public function addRole(UserInterface $utilisateur, RoleInterface $role): UserInterface
    {
        $utilisateur->addRole($role);

        try {
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de l'ajout du rôle \"" . $role->getLibelle() . "\" à l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    public function removeRole(UserInterface $utilisateur, RoleInterface $role): UserInterface
    {
        $utilisateur->removeRole($role);

        try {
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression du rôle \"" . $role->getLibelle() . "\" à l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    public function getConnectedUser(): ?UserInterface
    {
        $identity = $this->authenticationService->getIdentity();

        if ($identity) {
            if (isset($identity['ldap'])) {
                /** @var People $userIdentity */
                $userIdentity = $identity['ldap'];
                $username = match ($this->getAuthentificationConfig()['local']['ldap']['username']) {
                    'uid' => $userIdentity->getUid(),
                    'supannaliaslogin' => $userIdentity->getSupannAliasLogin(),
                    default => throw new RuntimeException("La clef de config [unicaen-auth][loca][ldap][username] n'est pas défini ou à une valeur non attendue."),
                };

                $user = $this->findByUsername($username);

                return $user;
            }
            if (isset($identity['shib'])) {
                /** @var People $userIdentity */
                $userIdentity = $identity['shib'];
                $username = $userIdentity->getUsername();
                $user = $this->findByUsername($username);

                return $user;
            }
            if (isset($identity['db'])) {
                return $identity['db'];
            }
        }

        return null;
    }

    /**
     * @return \Laminas\Permissions\Acl\Role\RoleInterface|null
     */
    public function getConnectedRole()
    {
        $dbRole = $this->serviceUserContext->getSelectedIdentityRole();
        return $dbRole;
    }

     /** @return UserInterface[] **/
    public function getUtilisateursByRoleIdAsOptions(?string $roleId = null): array
    {
        $role = $this->roleService->findByRoleId($roleId);
        $users = $this->findByRole($role);

        $array = [];
        foreach ($users as $user) {
            $array[$user->getId()] = $user->getDisplayName();
        }
        return $array;
    }

    /**
     * @param UserInterface[] $users
     * @return array
     */
    public function formatUserJSON(array $users): array
    {
        $result = [];

        /** @var User[] $responsables */
        foreach ($users as $user) {
            $result[] = array(
                'id' => $user->getId(),
                'label' => $user->getDisplayName(),
                'extra' => "<span class='badge' style='background-color: slategray;'>" . $user->getEmail() . "</span>",
            );
        }
        usort($result, function ($a, $b) {
            return strcmp($a['label'], $b['label']);
        });

        return $result;
    }

    /** TODO FAIRE MIEUX */
    public function getMaxId(): int
    {
        /** @var User $users */
        $users = $this->getRepo()->findAll();
        $maximum = 0;

        foreach ($users as $user) {
            if ($user->getId() > $maximum) $maximum = $user->getId();
        }
        return $maximum;
    }

    public function createFromSearchData(string $userId): ?UserInterface
    {
        list($source, $id) = explode('||', $userId);
        $sourceUser = $this->searchServices[$source]->findById($id);
        $newUser = $this->importFromRechercheIndividuResultatInterface($sourceUser, $source);
        $user = $this->findByUsername($newUser->getUsername($this->usernamefield));

        if (!$user) {
            $user = $this->create($newUser);
        }
        return $user;

    }
}

