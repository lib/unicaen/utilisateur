<?php

namespace UnicaenUtilisateur\Service\User;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\View\Renderer\PhpRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\UserContext;
use UnicaenMail\Service\Mail\MailService;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateur\Service\Role\RoleService;
use Laminas\Authentication\AuthenticationService;

class UserServiceFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) : UserService
    {
        /**
         * @var EntityManager $entityManager
         * @var MailService $mailService
         * @var RoleService $roleService
         * @var UserContext $userContext
         * @var AuthenticationService $authenticationService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $mailService = $container->get(MailService::class);
        $roleService = $container->get(RoleService::class);
        $userContext = $container->get(UserContext::class);
        $allConfig = $container->get('Config');
        $authenticationService = $container->get('Laminas\Authentication\AuthenticationService');

        $service = new UserService();
        $service->setEntityManager($entityManager);
        $service->setMailService($mailService);
        $service->setRoleService($roleService);
        $service->setServiceUserContext($userContext);
        $service->setUserEntityClass($allConfig['zfcuser']['user_entity_class'] ?? User::class);
        $service->setAuthentificationConfig($allConfig['unicaen-auth']);
        $service->setAuthenticationService($authenticationService);

        /* @var PhpRenderer $renderer  */
        $renderer = $container->get('ViewRenderer');
        $service->setRenderer($renderer);

        $config = $container->get('Config');
        $appname = $config['unicaen-app']['app_infos']['nom'];
        $service->setAppname($appname);

        // services de recherche d'un individu : application, ldap, octopus,...
        $rechercheServices = [];
        if(isset($config['unicaen-utilisateur']['recherche-individu']) && !empty($config['unicaen-utilisateur']['recherche-individu'])) {
            foreach ($config['unicaen-utilisateur']['recherche-individu'] as $key => $service_) {

                //sinon boucle ...
                if ($key === 'app') $serviceR = $this;
                else  $serviceR = $container->get($service_);

                if($serviceR instanceof RechercheIndividuServiceInterface) {
                    $rechercheServices[$key] = $serviceR;
                }
            }
        }
        $service->setSearchServices($rechercheServices);

        //Récupuration du cout des mots de passe pour le chiffrements
        $options = array_merge(
            $container->get('zfcuser_module_options')->toArray(),
            $container->get('unicaen-auth_module_options')->toArray());
        $moduleOptions = new ModuleOptions($options);
        $service->setPasswordCost($moduleOptions->getPasswordCost());

        return $service;
    }
}