<?php

namespace UnicaenUtilisateur\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Form\Role\RoleForm;
use UnicaenUtilisateur\Service\Role\RoleService;

class RoleControllerFactory {

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RoleController|object
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        /**
         * @var RoleService $roleService
         */
        $roleService = $container->get(RoleService::class);
        $config = $container->get('Config');
        $roleForm = $container->get('FormElementManager')->get(RoleForm::class);

        $identityProviders = [];
        if ($config['unicaen-auth']['identity_providers'] !== "") {
            if (isset($config['unicaen-auth']['identity_providers'])) {
                $identityProviderPaths = $config['unicaen-auth']['identity_providers'];
                foreach ($identityProviderPaths as $identityProviderPath) $identityProviders[] = $container->get($identityProviderPath);
            }
        }

        $controller = new RoleController();
        $controller->setIdentityProviders($identityProviders);
        $controller->setRoleService($roleService);
        $controller->setRoleForm($roleForm);
        
        return $controller;
    }
}