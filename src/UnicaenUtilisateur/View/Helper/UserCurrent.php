<?php
namespace UnicaenUtilisateur\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue affichant toutes les infos concernant l'utilisateur connecté.
 * C'est à dire le nom de l'utilisateur connecté & son rôle courant.
 *
 * Lorsqu'on clique sur le nom, s'affiche un popover
 * présentant les rôles sélectionnables, des infos administratives, le formulaire d'usurpation (si habilité)
 * et le lien de déconnexion.
 *
 * Si aucun utilisateur n'est connecté, cette aide de vue renvoit ''.
 *
 * @author Unicaen
 *
 * @property PhpRenderer $view
 */
class UserCurrent extends UserAbstract
{
    /**
     * @var bool
     */
    protected $affectationFineSiDispo = false;

    protected bool $displayUserInfo = true;

    /**
     * Point d'entrée.
     * 
     * @param boolean $affectationFineSiDispo Indique s'il faut prendre en compte l'affectation
     * plus fine (ucbnSousStructure) si elle existe, à la place de l'affectation standard (niveau 2)
     * @return self 
     */
    public function __invoke($affectationFineSiDispo = false)
    {
        $this->setAffectationFineSiDispo($affectationFineSiDispo);
        return $this;
    }
    
    /**
     * Retourne le code HTML généré par cette aide de vue.
     * 
     * @return string 
     */
    public function __toString(): string
    {
        if (! $this->getIdentity()) {
            return '';
        }

        // - Nom de l'utilisateur connecté, le cas échéant.
        /* @var $userStatusHelper UserStatus */
        $userStatusHelper = $this->view->plugin('userStatus');
        $nom = $userStatusHelper(false);
        // - Rôle courant.
        $role = '';
        $userProfileSelectable = true;
        if ($userProfileSelectable) {
            $role = $this->getUserContext()->getSelectedIdentityRole();
            // cas où aucun rôle n'est sélectionné : on affiche le 1er rôle sélectionnable ou sinon "user"
            if ($role === null) {
                $selectableRoles = $this->getUserContext()->getSelectableIdentityRoles();
                $role = current($selectableRoles) ?: $this->getUserContext()->getIdentityRole('user');
            }
            $role = sprintf(
                "<br/><small class='role-libelle'>%s</small>",
                !method_exists($role, '__toString') ? $role->getRoleId() : $role
            );
        }

        // - Rôles sélectionnables.
        /* @var \UnicaenUtilisateur\View\Helper\UserProfile $userProfileHelper */
        $userProfileHelper = $this->view->plugin('userProfile');
        $userProfileHelper->setUserProfileSelectable($userProfileSelectable);
        $roles = (string) $userProfileHelper;

        // - Infos administratives.
        $infos = '';
        if ($this->isDisplayUserInfo()) {
            /* @var \UnicaenUtilisateur\View\Helper\UserInfo $userInfoHelper */
            $userInfoHelper = $this->view->plugin('userInfo');
            $infos = (string)$userInfoHelper($this->getAffectationFineSiDispo());
        }

        // - Usurpation d'identité.
        $usurpation = '';
        /** @var PhpRenderer $renderer */
        $renderer = $this->getView();
        // teste si userUsurpation de UnicaenAuthentification est dispo sans avoir de dépendance forte avec cette bib
        if ($renderer->getHelperPluginManager()->has('userUsurpation')) {
            /* @var \UnicaenAuthentification\View\Helper\UserUsurpationHelper $userUsurpationHelper */
            $userUsurpationHelper = $this->view->plugin('userUsurpation');
            $usurpation = (string)$userUsurpationHelper;
        }

        // - Lien de déconnexion.
        /** @var \UnicaenUtilisateur\View\Helper\UserConnection $userConnectionHelper */
        $userConnectionHelper = $this->getView()->plugin('userConnection');
        $deconnexion = $userConnectionHelper->addClass('btn btn-lg btn-outline-success')->renderDisconnection();

        // Dans un popover
        $linkText = $nom . $role;
        $popoverContent = htmlspecialchars(preg_replace('/\r\n|\n|\r/', '',
            $roles .
            $infos .
            $usurpation .
            '<hr>' .
            '<div class="text-center">' . $deconnexion . '</div>'
        ));

        $html = <<<EOS
<a class="navbar-link dropdown-toggle"
   id="user-current-info" 
   data-bs-placement="bottom" 
   data-bs-container="#navbar" 
   data-bs-content="$popoverContent" 
   onclick="return false"
   href="#">$linkText<span class="caret"></span></a>
EOS;

        $this->view->inlineScript()->appendScript(<<<EOS
$(function() {
    $("a#user-current-info").popover({
        html: true, 
        sanitize: false,
    });
});
EOS
);
        return $html;
    }

    /**
     * Indique si l'affichage de l'affectation fine éventuelle est activé ou non.
     * 
     * @return bool
     */
    public function getAffectationFineSiDispo()
    {
        return $this->affectationFineSiDispo;
    }

    /**
     * Active ou non l'affichage de l'affectation fine éventuelle.
     * 
     * @param bool $affectationFineSiDispo
     * @return self
     */
    public function setAffectationFineSiDispo($affectationFineSiDispo = true)
    {
        $this->affectationFineSiDispo = $affectationFineSiDispo;
        return $this;
    }

    public function isDisplayUserInfo(): bool
    {
        return $this->displayUserInfo;
    }

    public function setDisplayUserInfo(bool $displayUserInfo): UserCurrent
    {
        $this->displayUserInfo = $displayUserInfo;

        return $this;
    }
}
