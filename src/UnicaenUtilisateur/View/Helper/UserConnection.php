<?php
namespace UnicaenUtilisateur\View\Helper;

/**
 * Aide de vue dessinant :
 * - soit le lien de connexion à l'appli : {@see renderConnection()}.
 * - soit le lien de déconnexion  : {@see renderDisconnection()}.
 * - soit l'un ou l'autre selon qu'un utilisateur est connecté ou pas : {@see render()}.
 *
 * @author Unicaen
 */
class UserConnection extends UserAbstract
{
    protected $template = '<a class="%s" href="%s" title="%s">%s</a>';

    protected $classes = [
        'user-connection',
        //'navbar-link',
    ];

    public function __invoke(): self
    {
        return $this;
    }

    public function addClass(string $class): self
    {
        $this->classes[] = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    public function render(): string
    {
        if ($this->getIdentity()) {
            return $this->renderDisconnection();
        } else {
            return $this->renderConnection();
        }
    }

    /**
     * @return string Lien de connexion, ou '' si l'utilisateur est déjà connecté.
     */
    public function renderConnection(): string
    {
        if ($this->getIdentity()) {
            return '';
        }

        $urlHelper = $this->getView()->plugin('url');
        $href  = $urlHelper('zfcuser/login');
        $lib   = "Connexion";
        $title = "Cliquez pour vous authentifier";

        if ($this->getTranslator()) {
            $lib   = $this->getTranslator()->translate($lib, $this->getTranslatorTextDomain());
            $title = $this->getTranslator()->translate($title, $this->getTranslatorTextDomain());
        }

        $classes = implode(' ', $this->classes);

        return sprintf($this->template, $classes, $href, $title, $lib);
    }

    /**
     * @return string Lien de déconnexion, ou '' si aucun utilisateur n'est connecté.
     */
    public function renderDisconnection(): string
    {
        if (!$this->getIdentity()) {
            return '';
        }

        $urlHelper = $this->getView()->plugin('url');
        $href  = $urlHelper('zfcuser/logout');
        $lib   = "Déconnexion";
        $title = "Cliquez pour vous déconnecter";

        if ($this->getTranslator()) {
            $lib   = $this->getTranslator()->translate($lib, $this->getTranslatorTextDomain());
            $title = $this->getTranslator()->translate($title, $this->getTranslatorTextDomain());
        }

        $classes = implode(' ', $this->classes);

        return sprintf($this->template, $classes, $href, $title, $lib);
    }

    /**
     * @return string
     * @deprecated Utiliser {@see render()}
     */
    protected function createConnectionLink()
    {
        $identity = $this->getIdentity();

        $urlHelper = $this->getView()->plugin('url');

        $template = '<a class="btn btn-lg btn-outline-primary navbar-link user-connection" href="%s" title="%s">%s</a>';
        if (!$identity) {
            $href  = $urlHelper('zfcuser/login');
            $lib   = "Connexion";
            $title = "Cliquez pour vous authentifier";
        }
        else {
            $href  = $urlHelper('zfcuser/logout');
            $lib   = "Déconnexion";
            $title = "Supprime les informations de connexion";
        }
        if ($this->getTranslator()) {
            $lib   = $this->getTranslator()->translate($lib, $this->getTranslatorTextDomain());
            $title = $this->getTranslator()->translate($title, $this->getTranslatorTextDomain());
        }
        $link = sprintf($template, $href, $title, $lib);

        return $link;
    }
}