<?php

namespace UnicaenUtilisateur\Entity\Db;

use DateTime;
use Exception;
use UnicaenApp\Exception\RuntimeException;

trait HistoriqueAwareTrait
{
    protected ?DateTime $histoCreation = null;
    protected ?DateTime $histoModification = null;
    protected ?DateTime $histoDestruction = null;
    protected ?UserInterface $histoCreateur = null;
    protected ?UserInterface $histoModificateur = null;
    protected ?UserInterface $histoDestructeur = null;

    public function setHistoCreation(?DateTime $histoCreation): HistoriqueAwareInterface
    {
        $this->histoCreation = $histoCreation;
        return $this;
    }

    public function getHistoCreation(): ?DateTime
    {
        return $this->histoCreation;
    }

    public function setHistoDestruction(?DateTime $histoDestruction): HistoriqueAwareInterface
    {
        $this->histoDestruction = $histoDestruction;
        return $this;
    }

    public function getHistoDestruction(): ?DateTime
    {
        return $this->histoDestruction;
    }

    public function setHistoModification(?DateTime $histoModification): HistoriqueAwareInterface
    {
        $this->histoModification = $histoModification;
        return $this;
    }

    public function getHistoModification(): ?DateTime
    {
        return $this->histoModification;
    }

    public function setHistoModificateur(?UserInterface $histoModificateur = null): HistoriqueAwareInterface
    {
        $this->histoModificateur = $histoModificateur;
        return $this;
    }

    public function getHistoModificateur(): ?UserInterface
    {
        return $this->histoModificateur;
    }

    public function setHistoDestructeur(?UserInterface $histoDestructeur = null): HistoriqueAwareInterface
    {
        $this->histoDestructeur = $histoDestructeur;
        return $this;
    }

    public function getHistoDestructeur(): ?UserInterface
    {
        return $this->histoDestructeur;
    }

    public function setHistoCreateur(?UserInterface $histoCreateur = null): HistoriqueAwareInterface
    {
        $this->histoCreateur = $histoCreateur;
        return $this;
    }

    public function getHistoCreateur(): ?UserInterface
    {
        return $this->histoCreateur;
    }

    /** Historisation / Restauration **********************************************************************************/

    /**
     * @param UserInterface|null $destructeur Auteur de la suppression ; si null, peut être renseigné
     *                                            automatiquement (cf. HistoriqueListener) si la classe implémente
     *                                            HistoriqueAwareInterface
     * @param DateTime|null $dateDestruction Date éventuelle de la suppression
     * @return HistoriqueAwareInterface
     * @see HistoriqueAwareInterface
     */
    public function historiser(?UserInterface $destructeur = null, ?DateTime $dateDestruction = null): HistoriqueAwareInterface
    {
        if ($destructeur) {
            $this->setHistoDestructeur($destructeur);
        }

        if (empty($dateDestruction)) {
            try {
                $dateDestruction = new DateTime();
            } catch (Exception $e) {
                throw new RuntimeException("Impossible d'instancier un DateTime!", null, $e);
            }
        }

        $this->setHistoDestruction($dateDestruction);

        return $this;
    }

    public function dehistoriser(): HistoriqueAwareInterface
    {
        $this->setHistoDestructeur();
        $this->setHistoDestruction(null);

        return $this;
    }

    /**
     * @param DateTime|null $dateObs Date d'observation éventuelle
     * @return bool
     */
    public function estHistorise(DateTime $dateObs = null): bool
    {
        return !$this->estNonHistorise($dateObs);
    }

    /**
     * @param DateTime|null $dateObs Date d'observation éventuelle
     * @return bool
     */
    public function estNonHistorise(DateTime $dateObs = null): bool
    {
        if (empty($dateObs)) {
            $dateObs = new DateTime();
        }

        $dObs = $dateObs->format('Y-m-d');
        $dDeb = $this->getHistoCreation()?->format('Y-m-d');
        $dFin = $this->getHistoDestruction()?->format('Y-m-d');

        if ($dDeb && !($dDeb <= $dObs)) return false;
        if ($dFin && !($dObs < $dFin)) return false;

        return true;
    }

    public function derniereModification(): string
    {
        if ($this->getHistoDestruction() !== null) {
            return "Historisation " . $this->getHistoDestruction()->format('d/m/Y à H:i:s') . " par " . $this->getHistoDestructeur()->getDisplayName();
        }
        if ($this->getHistoModification() !== null) {
            return "Modification " . $this->getHistoModification()->format('d/m/Y à H:i:s') . " par " . $this->getHistoModificateur()->getDisplayName();
        }
        if ($this->getHistoCreation() !== null) {
            return "Création " . $this->getHistoCreation()->format('d/m/Y à H:i:s') . " par " . $this->getHistoCreateur()->getDisplayName();
        }
        return "Aucune donnée d'historisation";
    }

    public function getDateTime(): ?string
    {
        return $this->getHistoCreation()?->format('d/m/Y à H:i');
    }

    public function getDate(): ?string
    {
        return $this->getHistoCreation()?->format('d/m/Y');
    }
}