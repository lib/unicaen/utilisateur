<?php

namespace UnicaenUtilisateur\Entity\Db;

use DateTime;

/**
 * Adaptation de \UnicaenApp\Entity\Db\HistoriqueAwareInterface pour utiliser la UserInterface de UnicaenUtilisateur
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
interface HistoriqueAwareInterface
{
    public function setHistoCreation(?DateTime $histoCreation): HistoriqueAwareInterface;
    public function getHistoCreation(): ?DateTime;
    public function setHistoDestruction(?DateTime $histoDestruction): HistoriqueAwareInterface;
    public function getHistoDestruction(): ?DateTime;
    public function setHistoModification(?DateTime $histoModification): HistoriqueAwareInterface;
    public function getHistoModification(): ?DateTime;


    public function setHistoModificateur(?UserInterface $histoModificateur = null): HistoriqueAwareInterface;
    public function getHistoModificateur(): ?UserInterface;
    public function setHistoDestructeur(?UserInterface $histoDestructeur = null): HistoriqueAwareInterface;
    public function getHistoDestructeur(): ?UserInterface;
    public function setHistoCreateur(?UserInterface $histoCreateur = null): HistoriqueAwareInterface;
    public function getHistoCreateur(): ?UserInterface;


    public function estHistorise(?DateTime $dateObs = null) : bool;
    public function estNonHistorise(?DateTime $dateObs = null) : bool;
    public function historiser(?UserInterface $histoDestructeur, ?DateTime $histoDestruction = null): HistoriqueAwareInterface;
    public function dehistoriser(): HistoriqueAwareInterface;

    public function derniereModification(): string;
}