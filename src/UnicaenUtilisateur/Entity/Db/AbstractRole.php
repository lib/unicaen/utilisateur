<?php

namespace UnicaenUtilisateur\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenPrivilege\Entity\Db\PrivilegeInterface;

abstract class AbstractRole implements RoleInterface
{
    protected ?int $id = null;
    protected ?string $roleId = null;
    protected ?string $libelle = null;
    protected ?string $description = null;
    protected bool $default = false;
    protected bool $auto = false;
    protected ?RoleInterface $parent = null;
    protected ?string $ldapFilter = null;
    protected bool $accessibleExterieur = true;
    protected bool $displayed = true;

    protected Collection $users;
    protected Collection $privileges;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->privileges = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function setId(?int $id) : void
    {
        $this->id = (int)$id;
    }

    public function getRoleId() : ?string
    {
        return $this->roleId;
    }

    public function setRoleId(?string $roleId) : void
    {
        $this->roleId = (string)$roleId;
    }

    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle) : void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }


    public function isDefault() : bool
    {
        return $this->default;
    }

    public function setDefault(bool $default) : void
    {
        $this->default = (boolean) $default;
    }

    public function isAuto() : bool
    {
        return $this->auto;
    }

    public function setAuto(bool $auto) : void
    {
        $this->auto = $auto;
    }

    public function isAccessibleExterieur() : bool
    {
        return $this->accessibleExterieur;
    }

    public function setAccessibleExterieur(bool $accessibleExterieur) : void
    {
        $this->accessibleExterieur = $accessibleExterieur;
    }

    public function isDisplayed(): bool
    {
        return $this->displayed;
    }

    public function setDisplayed(bool $displayed): void
    {
        $this->displayed = $displayed;
    }

    public function getParent() : ?RoleInterface
    {
        return $this->parent;
    }

    public function setParent(?RoleInterface $parent = null) : void
    {
        $this->parent = $parent;
    }

    public function getLdapFilter() : ?string
    {
        return $this->ldapFilter;
    }

    public function setLdapFilter(?string $ldapFilter) : void
    {
        $this->ldapFilter = $ldapFilter;
    }

    public function getUsers() : Collection
    {
        return $this->users;
    }

    public function addUser(UserInterface $user) : void
    {
        $this->users->add($user);
    }

    public function removeUser(UserInterface $user) : void
    {
        $this->users->removeElement($user);
    }

    public function getPrivileges() : Collection
    {
        return $this->privileges;
    }

    public function addPrivilege(PrivilegeInterface $privilege) : void
    {
        $this->privileges->add($privilege);
    }

    public function removePrivilege(PrivilegeInterface $privilege) : void
    {
        $this->privileges->removeElement($privilege);
    }

    /** Fonctions magiques ********************************************************************************************/

    public function __toString() : string
    {
        return $this->getLibelle();
    }

}