<?php

namespace UnicaenUtilisateur\Form\Role;

use  Application\Entity\Db\UtilisateurRole;
use DoctrineModule\Form\Element\ObjectSelect;
use DoctrineModule\Validator\NoObjectExists;
use DoctrineModule\Validator\UniqueObject;
use Laminas\Form\Element\Textarea;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Form\Strategy\RoleParentStrategy;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\Regex;

class RoleForm extends Form
{
    use RoleServiceAwareTrait;
    use EntityManagerAwareTrait;

    public function init()
    {
        $this->setAttribute('id', 'form-role');

        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description :",
                'label_attributes' => [
                ],
            ],
            'attributes' => [
                'id' => 'description',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'roleId',
            'options' => [
                'label' => "Identifiant :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'role-id',
            ],
        ]);

        $this->add([
            'type' => ObjectSelect::class,
            'name' => 'parent',
            'options' => [
                'label' => "Rôle parent :",
                'empty_option' => "Aucun rôle parent",
                'object_manager' => $this->getEntityManager(),
                'target_class' => $this->roleService->getEntityClass(),
                'property' => 'libelle',
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy' => ['libelle' => 'ASC'],
                    ],
                ],
                'disable_inarray_validator' => true,
            ],
            'attributes' => [
                'id' => 'parent',
            ],
        ]);
        $strategy = new RoleParentStrategy();
        $strategy->setRoleService($this->getRoleService());
        $this->getHydrator()->addStrategy('parent', $strategy);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'default',
            'options' => [
                'label' => "rôle par défaut",
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ],
        ]);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'auto',
            'options' => [
                'label' => "rôle automatique",
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ],
        ]);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'accessibleExterieur',
            'options' => [
                'label' => "accès extérieur",
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ],
        ]);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'displayed',
            'options' => [
                'label' => "est affiché",
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'ldapFilter',
            'options' => [
                'label' => "Filtre LDAP :",
            ],
            'attributes' => [
                'id' => 'ldap-filter',
            ],
        ]);

        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un libellé."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Ce libellé est déjà utilisé pour un autre rôle.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $role = $this->roleService->findByLibelle($value);
                                if(!$role) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getLibelle()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],
            'description' => [
                'required' => false,
            ],
            'roleId' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un identifiant."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
//                    [
//                        'name' => Regex::class,
//                        'options' => [
//                            'pattern' => '/^[a-z0-9_-]+$/',
//                            'messages' => [
//                                Regex::NOT_MATCH => "Seuls les caractères suivants sont autorisés : [a-z, 0-9, _, -].",
//                            ],
//                            'break_chain_on_failure' => true,
//                        ],
//                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Cet identifiant est déjà utilisé pour un autre rôle.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $role = $this->roleService->findByRoleId($value);
                                if(!$role) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getRoleId()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

            'parent' => [
                'required' => false,
            ],
        ]));
    }
}
