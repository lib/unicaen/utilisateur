<?php

namespace  UnicaenUtilisateur\Form\Role;

use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use UnicaenUtilisateur\Service\Role\RoleService;

class RoleFormFactory
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RoleForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var RoleService $roleService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $roleService = $container->get(RoleService::class);
        $hydrator = $container->get('HydratorManager')->get(DoctrineObject::class);

        $form = new RoleForm();
        $form->setEntityManager($entityManager);
        $form->setRoleService($roleService);
        $form->setHydrator($hydrator);

        return $form;
    }
}
