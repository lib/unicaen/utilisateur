<?php

namespace UnicaenUtilisateur\Form\User;

use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use UnicaenUtilisateur\Service\User\UserService;

class UserFormFactory
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var UserService $userService
         * @var UserHydrator $hydrator
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $userService = $container->get(UserService::class);
        $hydrator = $container->get('HydratorManager')->get(UserHydrator::class);
        
        $form = new UserForm();
        $form->setEntityManager($entityManager);
        $form->setUserService($userService);
        $form->setHydrator($hydrator);

        return $form;
    }
}
