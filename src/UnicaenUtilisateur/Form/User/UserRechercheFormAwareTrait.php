<?php

namespace UnicaenUtilisateur\Form\User;

trait UserRechercheFormAwareTrait
{
    /**
     * @var UserRechercheForm
     */
    protected $userRechercheForm;


    /**
     * @param UserRechercheForm $userRechercheForm
     * @return UserRechercheForm
     */
    public function setUserRechercheForm(UserRechercheForm $userRechercheForm) : UserRechercheForm
    {
        $this->userRechercheForm = $userRechercheForm;

        return $this->userRechercheForm;
    }

    /**
     * @return UserRechercheForm
     */
    public function getUserRechercheForm() : UserRechercheForm
    {
        return $this->userRechercheForm;
    }
}