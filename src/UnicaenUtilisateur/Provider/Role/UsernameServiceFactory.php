<?php

namespace UnicaenUtilisateur\Provider\Role;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of UsernameServiceFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UsernameServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('BjyAuthorize\Config');

        if (! isset($config['role_providers']['UnicaenUtilisateur\Provider\Role\Username'])) {
            throw new \InvalidArgumentException(
                'Config for "UnicaenUtilisateur\Provider\Role\Username" not set'
            );
        }

        $providerConfig = $config['role_providers']['UnicaenUtilisateur\Provider\Role\Username'];

        $authService = $container->get('zfcuser_auth_service'); /* @var $authService \Laminas\Authentication\AuthenticationService */
//        $authService = $container->get('Laminas\Authentication\AuthenticationService'); /* @var $authService \Laminas\Authentication\AuthenticationService */

        return new Username($authService, $providerConfig);
    }
}