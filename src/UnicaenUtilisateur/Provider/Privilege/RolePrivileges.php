<?php

namespace UnicaenUtilisateur\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class RolePrivileges extends Privileges
{
    const ROLE_AFFICHER     = 'role-role_afficher';
    const ROLE_EFFACER      = 'role-role_effacer';
    const ROLE_MODIFIER     = 'role-role_modifier';
}