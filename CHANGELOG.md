Journal des modifications
=========================

7.0.2 (29/01/2025)
------------------

- Dépendance ZfcUser explicite
- Remplacement de Laminas\Crypt\Password\Bcrypt par ZfcUser\Password\Bcrypt



7.0.1
-----
- [FIX] HistoriqueAwareTrait : méthodes getDateTime() et getDate() pas cohérentes avec getHistoCreation().

7.0.0
-----
- [Fix] Correction du problème de UserServiceFactory faisant appel à une méthode inconnue "setUsernameField" dans UserService
- Utilisation de la nouvelle version de unicaen/mail (symfony)

6.3.1
-----
- [FIX] HistoriqueAwareTrait : méthodes getDateTime() et getDate() pas cohérentes avec getHistoCreation().

6.3.0
-----
- Rattrapage des évolutions dans unicaen/auth : aides de vues User*
- Correction du .gitlab-ci.yml : l'utilisation d'une image python suffit pour la màj satis
- [FIX] HistoriqueListener : prise en compte nécessaire du paramètre de config 'user_entity_class'.
- [FIX] Popover affichant le formulaire de sélection du rôle : initialisation dédiée avec option 'sanitize' à true indispensable 

6.0.4 (21/03/2023)
-----
- Ajout du paramètre [display-user-info](config/unicaen-utilisateur.global.php.dist) qui permet d'afficher ou nonles affectations LDAP de l'utilisateur courant
- [Fix] Test de présence de 'userUsurpation' pour couplage faible avec UnicaenAuthentification

5.0.8
------
* Correction du formulaire des rôles (la decsription n'est plus obligatoire)
* Création d'une classe abstraite AbstractIdentityProvider pour somplifire la déclaration des IdentityProviders
* Ajout de garde à l'appel des fonctions de gestion des rôles automatiques 

5.0.7 
------
* Ajout du champs description pour les rôles
* Ajout d'un view helper pour l'affichage des rôles ```echo $this->role($role)```
* Début de nettoyage des classes vers la syntaxe PHP8


0.0.2 (29/03/2021)
-----
- [Fix] Correction exception incomplete dans UserService

0.0.1 (12/03/2021)
-----

Version initiale du module permettant :
- la gestion des utilisateurs
- la gestion des rôles
- l'affectation de rôles à des utilisateurs
- l'export des utilisateurs
- les exports des utilisateurs ayant un rôle donné 
