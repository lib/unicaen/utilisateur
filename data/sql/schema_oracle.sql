-- -----------------------------------------------------
-- TABLE UNICAEN_UTILISATEUR_ROLE
-- -----------------------------------------------------
CREATE SEQUENCE UNICAEN_UTILISATEUR_ROLE_ID_SEQ NOCYCLE START WITH 1 INCREMENT BY 1 MINVALUE 0;

CREATE TABLE UNICAEN_UTILISATEUR_ROLE (
  ID                    NUMBER(*, 0)    NOT NULL,
  ROLE_ID               VARCHAR2(64)    NOT NULL,
  LIBELLE               VARCHAR2(255)   NOT NULL,
  DESCRIPTION           CLOB            NOT NULL,
  IS_DEFAULT            NUMBER(1)       DEFAULT 0 NOT NULL,
  IS_AUTO               NUMBER(1)       DEFAULT 0 NOT NULL,
  PARENT_ID             NUMBER(*, 0),
  LDAP_FILTER           VARCHAR2(255),
  ACCESSIBLE_EXTERIEUR  NUMBER(1)       DEFAULT 1 NOT NULL,
  DISPLAYED             NUMBER(1)       DEFAULT 1 NOT NULL,
  CONSTRAINT PK_UNICAEN_UTILISATEUR_ROLE PRIMARY KEY (ID),
  CONSTRAINT UN_UNICAEN_UTILISATEUR_ROLE_ROLE UNIQUE (ROLE_ID),
  CONSTRAINT FK_UNICAEN_UTILISATEUR_ROLE_PARENT FOREIGN KEY (PARENT_ID) REFERENCES UNICAEN_UTILISATEUR_ROLE (ID) DEFERRABLE INITIALLY IMMEDIATE
);

CREATE INDEX IX_UTILISATEUR_ROLE_PARENT ON UTILISATEUR_ROLE (PARENT_ID);


-- -----------------------------------------------------
-- TABLE UNICAEN_UTILISATEUR_USER
-- -----------------------------------------------------
CREATE SEQUENCE UNICAEN_UTILISATEUR_USER_ID_SEQ NOCYCLE START WITH 1 INCREMENT BY 1 MINVALUE 0;

CREATE TABLE UNICAEN_UTILISATEUR_USER (
  ID                    NUMBER(*, 0)       NOT NULL,
  USERNAME              VARCHAR2(255 CHAR) NOT NULL,
  DISPLAY_NAME          VARCHAR2(255 CHAR) NOT NULL,
  EMAIL                 VARCHAR2(255 CHAR) NULL,
  PASSWORD              VARCHAR2(128 CHAR) NOT NULL,
  STATE                 SMALLINT DEFAULT 1 NOT NULL,
  PASSWORD_RESET_TOKEN  VARCHAR2(256) DEFAULT NULL,
  LAST_ROLE_ID          NUMBER(*,0),
  CONSTRAINT PK_UNICAEN_UTILISATEUR_USER PRIMARY KEY (ID),
  CONSTRAINT UN_UNICAEN_UTILISATEUR_USER_USERNAME UNIQUE (USERNAME),
  CONSTRAINT UN_UNICAEN_UTILISATEUR_USER_PASSWORD_RESET_TOKEN UNIQUE (PASSWORD_RESET_TOKEN),
  CONSTRAINT FK_UNICAEN_UTILISATEUR_USER_LAST_ROLE FOREIGN KEY (LAST_ROLE_ID) REFERENCES UNICAEN_UTILISATEUR_ROLE(ID) DEFERRABLE INITIALLY IMMEDIATE
);

CREATE INDEX IX_UNICAEN_UTILISATEUR_USER_LAST_ROLE ON UNICAEN_UTILISATEUR_USER(LAST_ROLE_ID);


-- -----------------------------------------------------
-- TABLE UNICAEN_UTILISATEUR_ROLE_LINKER
-- -----------------------------------------------------
CREATE TABLE UNICAEN_UTILISATEUR_ROLE_LINKER (
  UTILISATEUR_ID  NUMBER(*, 0) NOT NULL,
  ROLE_ID         NUMBER(*, 0) NOT NULL,
  CONSTRAINT PK_UNICAEN_UTILISATEUR_ROLE_LINKER PRIMARY KEY (UTILISATEUR_ID, ROLE_ID),
  CONSTRAINT FK_UNICAEN_UTILISATEUR_ROLE_LINKER_USER FOREIGN KEY (UTILISATEUR_ID) REFERENCES UNICAEN_UTILISATEUR_USER (ID) DEFERRABLE INITIALLY IMMEDIATE,
  CONSTRAINT FK_UNICAEN_UTILISATEUR_ROLE_LINKER_ROLE FOREIGN KEY (ROLE_ID) REFERENCES UNICAEN_UTILISATEUR_ROLE (ID) DEFERRABLE INITIALLY IMMEDIATE
);

CREATE INDEX IX_UNICAEN_UTILISATEUR_ROLE_LINKER_USER ON UNICAEN_UTILISATEUR_ROLE_LINKER (UTILISATEUR_ID);
CREATE INDEX IX_UNICAEN_UTILISATEUR_ROLE_LINKER_ROLE ON UNICAEN_UTILISATEUR_ROLE_LINKER (ROLE_ID);


-- DATA

INSERT INTO UNICAEN_UTILISATEUR_USER (
  ID,
  USERNAME,
  EMAIL,
  DISPLAY_NAME,
  PASSWORD)
WITH tmp AS (
  SELECT 1, 'app' u, 'dsi.applications@unicaen.fr'  e, 'Application'  n, 'application' p FROM dual
)
SELECT
  UNICAEN_UTILISATEUR_USER_ID_SEQ.nextval,
  tmp.u,
  tmp.e,
  tmp.n,
  tmp.p
FROM tmp;