Module Unicaen Utilisateur
=======================
------------------------

Changelogs
==========

[Journal des modifications](CHANGELOG.md)

Description
=======================

Le module *unicaen/utilisateur* fournit les mécanisme de gestion : 
* des utilisateurs : crud et export
* des rôles : crud et export
* des affections de rôles à des utilisateurs

Gestion des utilisateurs
=======================

Entités et représentation en base
---------------------------------

AbstractUser, User et UserInterface

Menu et GUI
------------

Administration > Utilisateur
Interface de recherche et création

Script creation/insert/privilege

Gestion des rôles
=======================

Entités et représentation en base
---------------------------------

AbstractRole, Role et RoleInterface

Menu et GUI
------------

Administration > Rôle
Interface de recherche et création

Script creation/insert/privilege

Configuration du module
-----------------------

Adapatateur Ldap et Octopus
---------------------------

La configuration du module est associé à la recherche d'individu dans d'autres services.
```php
 <?php
 
 use Octopus\Service\Individu\IndividuService;
 use UnicaenUtilisateur\Service\OtherSources\LdapService;
 use UnicaenUtilisateur\Service\User\UserService;
 
 return [
    'zfcuser' => [
        'user_entity_class' => \UnicaenUtilisateur\Entity\Db\User::class,
    ],
    'unicaen-auth' => [
        'role_entity_class' => \UnicaenUtilisateur\Entity\Db\Role::class,
    ],

     'unicaen-utilisateur' => [
         'recherche-individu' => [
             'app'       => UserService::class,
             'ldap'      => LdapService::class,
             'octopus'   => IndividuService::class,
         ],
     ],
 ];
```

La clef *recherche-individu* stocke un tableau contenant un lisiting de service implementant l'interface **RechercheIndividuServiceInterface** permettant la recherche d'utilisateur.
Cette interface force l'implémentation de deux méthodes :
1. *findById($id)* : qui retourne un **RechercheIndividuResultatInteface** associé à l'individu d'identifiant *$id*;
1. *findByTerm($term)* : qui retroune un tableau de **RechercheIndividuResultatInteface** associés au terme *$term*.

L'interface **RechercheIndividuResultatInteface** force l'implementation de quatre getters : *getId()*, *getUsername()*, *getDisplayname()* et *getEmail()*.

Les spaggethi
-------------


# Dépendences (ce qui pourra changer par le futur)

Le module réfère pour le moment : 
1. la bibliothèque **unicaen/auth** pour :
* les *Guard* des privilèges;
* les *LDAP/People*;
* les rôles de base *RoleInterface*; 
* les utilisateurs de base *UserInterface*;
* les service de recupération *UserContextService*, *UserContext*, *UserContextAwareTrait*;
* les événements de connexion *AuthenticatedUserSavedAbstractListener*, *UserAuthenticatedEvent*; 
2. la bibliothèque **unicaen/app** pour :
* les *SearchAndSelect*;
* le trait *EntityManagerAwareTrait*;
* le *CSVModel*;

L'affichage du listing des utilisateurs utilise DataTable. 
***Remarque :*** En cas d'oubli de cette bibliothèque le listing sera affiché comme un grand tableau.

## Aménagements

HistoriqueAwareTrait a été décallé dans Unicaen/Utilisateur. Il faut penser à changer les **use** associés de

```php  
use UnicaenApp\Entity\HistoriqueAwareTrait;
```
en
```php  
use UnicaenApp\Entity\HistoriqueAwareTrait;
``` 

ToDoes & Questions
------------------

* vérifier les listeners associé à l'historique.
    *  documenter ici usage
    *  generaliser usage pour retirer `GestionEntiteHistorisationTrait`
* Les aides de vue du **ConnectionApp** sont-elles à la bonne place ici ? 
    * ne devraient-elles pas être dans **UnicaenAuthentification** ?
    * changement de rôle et listeners associés (moins clair ici) ...
